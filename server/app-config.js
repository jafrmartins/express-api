module.exports = {
  debug: process.env.NODE_ENV != 'production' || true,
  port: process.env.PORT || process.env.HTTP_PORT || 3000,
  http_access_control: {
    origin: process.env.HTTP_ALLOW_ORIGIN || '*',
    methods: process.env.HTTP_ALLOW_METHODS || 'GET,PUT,POST,DELETE',
    headers: process.env.HTTP_ALLOW_HEADERS || 'Content-type,Authorization',
  },
  jwt: {
    secret: process.env.JWT_TOKEN_SECRET || 'secret',
    max_age: process.env.JWT_TOKEN_MAX_AGE || 3600,
    refresh_token_length: 16
  },
  database: {
    mysql: {
      host: process.env.MYSQL_HOST || 'localhost',
      user: process.env.MYSQL_USER || 'root',
      password: process.env.MYSQL_PASSWORD || '',
      database: process.env.MYSQL_DATABASE || 'database',
      connectionLimit: process.env.MYSQL_CONNECTION_LIMIT || 10
    }
  }
}