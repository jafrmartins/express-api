const Joi = require('@hapi/joi')

module.exports = {
  auth: {
    register: {
      body: Joi.object().keys({
        email: Joi.string().email({ minDomainSegments: 2 }).required(),
        password: Joi.string().regex(/^[a-zA-Z0-9]{4,16}$/).required(),
      })
    },
    verify: {
      query: Joi.object().keys({
        token: Joi.string().regex(/^[a-zA-Z0-9]{128}$/).required(),
      })
    },
    login: {
      body: Joi.object().keys({
        email: Joi.string().email({ minDomainSegments: 2 }).required(),
        password: Joi.string().regex(/^[a-zA-Z0-9]{4,16}$/).required(),
      })
    },
    refresh: {
      body: Joi.object().keys({
        token: Joi.string().regex(/^[a-zA-Z0-9]{128}$/).required(),
        email: Joi.string().email({ minDomainSegments: 2 }).required(),
      })
    },
    grant: {
      body: Joi.object().keys({
        grants: Joi.array().valid()
      })
    },
  },
  api: {
    users: {
      has_grants: {
        params: Joi.object().keys({
          id: Joi.number().required()
        })
      }
    },
    grants: {
      has_users: {
        params: Joi.object().keys({
          id: Joi.number().required()
        })
      }
    }
  }
}