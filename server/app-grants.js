const $me = (req) => {
  if(req.params.me) {
    return JSON.parse(req.params.me) == req.user.id  
  } return false
}

module.exports = { $me }