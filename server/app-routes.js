/**
 * Routes
 * Note: the order of the routes is relevant. Most Generic routes at the bottom.
 * Note: computed grants ($ prefix) should always come after regular grants
 */

// MIDDLEWARE
const events = require("./lib/middleware/events")
const { token, allow, deny } = require("./lib/middleware/jwt-auth")
const { callback: mysql } = require("./lib/middleware/mysql")
const validate = require("./lib/middleware/validate")


const e = require("./app-events")
const q = require("./app-queries")
const v = require("./app-validate")

module.exports = [
  
  /* AUTH */

  { "method": "post", "url": "/auth/register", "middleware": [
      events(e), validate(v.auth.register)
    ],"callback": "./server/middleware/auth#register" },
  { "method": "get", "url": "/auth/verify", "middleware": [
      validate(v.auth.verify)
    ], "callback": "./server/middleware/auth#verify" },
  { "method": "get", "url": "/auth/password", "middleware": [
      validate(v.auth.password)
    ], "callback": "./server/middleware/auth#password" },
  { "method": "post", "url": "/auth/reset", "middleware": [
      validate(v.auth.reset)
    ], "callback": "./server/middleware/auth#reset" },
  { "method": "post", "url": "/auth/login", "middleware": [
      validate(v.auth.login)
    ], "callback": "./server/middleware/auth#login" },
  { "method": "post", "url": "/auth/refresh", "middleware": [
      validate(v.auth.refresh)
    ], "callback": "./server/middleware/auth#refresh" },
  { "method": "post", "url": "/auth/grant", "middleware": [
      token, allow(["admin"], validate(v.auth.grant))
    ], "callback": "./server/middleware/auth#grant" 
  },
  
  /* CRUD */
  
  { "method": "get", "url": "/crud/:table/:id", "middleware": [
      token, allow(["admin"])
    ], "callback": mysql(q.crud.retrieve)
  },
  { "method": "put", "url": "/crud/:table/:id", "middleware": [
      token, allow(["admin"])
    ], "callback": mysql(q.crud.update)
  },
  { "method": "delete", "url": "/crud/:table/:id", "middleware": [
      token, allow(["admin"])
    ], "callback": mysql(q.crud.delete)
  },
  { "method": "post", "url": "/crud/:table", "middleware": [
      token, allow(["admin"])
    ], "callback": mysql(q.crud.create)
  },
  { "method": "get", "url": "/crud/:table", "middleware": [
      token, allow(["admin"])
    ], "callback": mysql(q.crud.retrieve)
  },

  /* API */

  { "method": "get", "url": "/api/grants/:id/users", "middleware": [
      token, allow(["admin"]), validate(v.api.grants.has_users)
    ], "callback": mysql(q.api.grants.has_users)
  },

  { "method": "get", "url": "/api/users/:me/grants", "middleware": [
      token, allow(["admin", "$me"]), validate(v.api.users.has_grants)
    ], "callback": mysql(q.api.users.has_grants)
  },

]