module.exports = {
  crud: {
    create: `
      INSERT INTO <%= params.table %> (<%= Object.keys(body).join(',') %>) 
      VALUES (<%= Object.values(body).map((val)=>{ return mysql.pool.escape(val) }).join(',') %>)
    `,
    retrieve: `
      SELECT <%= (query.fields) ? query.fields.split(',').join(',') : '*' %> 
      FROM <%= params.table %>
      <% if(params.id) { %>
      WHERE <%= params.table %>.id = <%=params.id%>
      <% } %>
      <% if(query.limit) { %>
      LIMIT <%= query.limit %>
      <% } %>
      <% if(query.offset) { %>
      OFFSET <%= query.offset %>
      <% } %>
    `,
    update: `
      UPDATE <%= params.table %> SET <%= Object.keys(body).map((key) => {
        return key+"="+mysql.pool.escape(body[key])
      }).join(',') %>
      <% if(params.id) { %>
      WHERE <%= params.table %>.id = <%= params.id %>
      <% } %>
    `,
    delete: `
      DELETE FROM <%= params.table %>
      <% if(params.id) { %>
      WHERE <%= params.table %>.id = <%= params.id %>
      <% } %>
    `
  },
  auth: {
    get_user_by_email: `
      SELECT users.id, users.email, users.password, users.salt
      FROM users
      WHERE users.email = <%=body.email%>
    `,
    get_user_grants: `
      SELECT grants.id, grants.name
      FROM users_has_grants
      LEFT JOIN users ON users.id = users_has_grants.users_id
      LEFT JOIN grants ON grants.id = users_has_grants.grants_id
      WHERE users.id = <%=user.id%>
    `,
    expire_token: `
      DELETE FROM user_tokens
      WHERE user_tokens.users_id = <%=user.id%> 
      AND user_tokens.token = <%=body.token%>
    `,
    create_token: `
      INSERT INTO user_tokens (users_id, token)
      VALUES (<%=user.id%>, <%=params.token%>)
    `,
    user_has_grants: `
      
    `
  },
  api: {
    users: {
      has_grants: `
        SELECT users.email, grants.name 
        FROM users_has_grants 
        LEFT JOIN users ON users_has_grants.users_id = users.id
        LEFT JOIN grants ON users_has_grants.grants_id = grants.id
        WHERE grants.id = <%=params.id%>
        GROUP BY grants.name
        LIMIT <%=query.limit%> OFFSET <%=query.offset%>
      `
    },
    grants: {
      has_users: `
        SELECT grants.id, grants.name 
        FROM users_has_grants 
        LEFT JOIN users ON users_has_grants.users_id = users.id
        LEFT JOIN grants ON users_has_grants.grants_id = grants.id
        WHERE users.id = <%=params.me%>
        LIMIT <%=query.limit%> OFFSET <%=query.offset%>
      `
    }
  }
}