const crypto = require('crypto')

const randomToken = (length=4) => {
  return crypto.randomBytes(length).toString('hex')
};

const saltHash = ({ password, salt = randomToken()}) => {
  const hash = crypto
    .createHmac('sha512', salt)
    .update(password)
  return {
    salt,
    hash: hash.digest('hex')
  }
};

module.exports = { randomToken, saltHash }