const jwt = require('jsonwebtoken')
const { jwt: config } = require('../../app-config')

const create_token = (user) => {
  let token = jwt.sign({
     data: {
       id: user.id,
       email: user.email,
       grants: user.grants
     }
    }, config.secret, {
      expiresIn: config.max_age,
      algorithm: 'HS256'
  })
  return token
}

const verify_token = (token) => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, config.secret, (err, decodedToken) => {
      if (err || !decodedToken) {
        return reject(err)
      } resolve(decodedToken)
    })
  })
}

module.exports = { create_token, verify_token }