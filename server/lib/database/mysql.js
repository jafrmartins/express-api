const mysql = require('mysql')
const util = require('util')

const pool = (config) => {
    const _pool = mysql.createPool(config)
    _pool.getConnection((err, connection) => {
        if (err) {
            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                console.error('Database connection was closed.')
            }
            if (err.code === 'ER_CON_COUNT_ERROR') {
                console.error('Database has too many connections.')
            }
            if (err.code === 'ECONNREFUSED') {
                console.error('Database connection was refused.')
            }
        }
        if (connection) connection.release()
        return
    })
    _pool.query = util.promisify(_pool.query)
    return _pool
}

const connection = (config) => {
    const _connection = mysql.createConnection(config)
    _connection.query = util.promisify(_connection.query)
    return _connection
}

module.exports = { pool, connection }