const t = require('lodash.template')
const { pool, connection } = require("../database/mysql")

const callback = (query, connection=null, compile=true) => {
  return async (req, res, next) => {
    try {
      let results
      let q = query
      if(compile) q = t(query)(req)
      if(connection) {
        const conn = req.mysql.connection(connection)
        results = await conn.query(q)
        conn.end()
      } else {
        results = await req.mysql.pool.query(q)
      } res.status(200).json(results)
    } catch (error) {
      next(error)
    }
  }
}

const middleware = (config) => {
  return (req, res, next) => {
    req.mysql = {
      connection,
      pool: pool(config)
    } 
    next()
  }
}

module.exports = { callback, middleware }