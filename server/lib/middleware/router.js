const { Router } = require('express')
const { resolve } = require('path')

module.exports = (routes) => {
  const app = new Router()
  for (let i = 0; i < routes.length; i++) {
    const R = new Router
    const route = routes[i];
    if(route.middleware) {
      route.middleware.map((mw) => {
        R[route.method](route.url, mw)
      })
    }
    let callback = route.callback
    if (typeof callback == "string") {
      let parts = route.callback.split("#")
      callback = require(resolve(process.cwd(), parts.shift()))
      if (exp = parts.shift()) { callback = callback[exp] }
    }
    if (typeof callback != "function") { 
      throw new Error(`invalid route ${JSON.stringify(route, null, 2)}`)
    }
    R[route.method](route.url, callback)
    app.use(R)
  }
  return app
}