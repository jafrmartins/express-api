module.exports = (events) => {
  return (req, res, next) => {
    req.events = events
    next()
  }
}