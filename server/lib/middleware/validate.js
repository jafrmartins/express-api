const Joi = require("@hapi/joi")
const { badRequest } = require("@hapi/boom")

module.exports = (_schema) => {

  return (req, res, next) => {

    let error = undefined
    for (const key of Object.keys(_schema)) {
      let data = req[key]
      let schema = _schema[key]
      const result = Joi.validate(schema, data)
      if (result.error) { 
        error = result.error 
        break
      } else {
        req[key] = result.value
      }
    }

    if(error) {
      return res.status(400).json(badRequest('invalid data', error))
    } next()

  }
}