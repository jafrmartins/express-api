const { badRequest, unauthorized } = require('@hapi/boom')
const { verify_token } = require('../utils/jwt')
const appGrants = require('../../app-grants')

const token = (req, res, next) => {
  if (req.headers.authorization) {
    let parts = req.headers.authorization.split(' ');
    if (parts.length == 2) {
      let scheme = parts[0];
      let token = parts[1];
      if (/^Bearer$/i.test(scheme)) {
        verify_token(token).then((decoded) => {
          req.user = decoded.data
          if (req.user.id) {
            next()
          } else  {
            res.status(400).json(badRequest('invalid token'))
          }
        }).catch((err) => {
          res.status(400).json(badRequest('invalid token'))
        })
      }
    } else {
      res.status(400).json(badRequest('invalid format (format is Authorization: Bearer [token])'))
    }
  } else {
    res.status(401).json(unauthorized('invalid request (format is Authorization: Bearer [token])'))
  }
}

const allow = (grants) => {
  return (req, res, next) => {
    if (!req.user.grants) {
      return res.status(401).json(unauthorized('insufficient permissions'))
    }
    let isAllowed = false
    for (let i = 0; i < grants.length; i++) {
      const grant = grants[i]
      if (req.user.grants.indexOf(grant) != -1) {
        isAllowed = true
        break
      } else if (/^\$/.test(grant) && typeof appGrants[grant] == 'function') {
        isAllowed = appGrants[grant](req)
        if (isAllowed) { break }
      } 
    }
    if (isAllowed) { return next() }
    res.status(401).json(unauthorized('insufficient permissions'))
  }
}

const deny = (grants) => {
  return (req, res, next) => {
    if (!req.user.grants) {
      return res.status(401).json(unauthorized('insufficient permissions'))
    }
    let isDenied = false
    for (let i = 0; i < grants.length; i++) {
      const grant = grants[i]
      if (req.user.grants.indexOf(grant) != -1) {
        isDenied = true
        break
      } else if (/^\$/.test(grant) && typeof appGrants[grant] == 'function') {
        isDenied = appGrants[grant](req)
        if (isDenied) { break }
      } 
    }
    if (!isDenied) { return next() }
    res.status(401).json(unauthorized('insufficient permissions'))
  }
}

module.exports = { allow, deny, token }