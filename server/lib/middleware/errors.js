module.exports = (config) => {
  return (error, req, res, next) => {
    if(error) {
      if (config.debug) {
        const { message, stack } = error
        res.status(500).json({ message, stack })
      } else {
        res.status(500).json({ message: "An error has ocurred please contact the system administrator" })
      }
    } else { next() }
  }
}