const { unauthorized, badImplementation, badRequest } = require('@hapi/boom')
const { auth } = require('../app-queries')

const register = async (req, res, next) => {
  
}

const verify = async (req, res, next) => {

}

const password = async (req, res, next) => {

}

const reset = async (req, res, next) => {

}


const login = async (req, res, next) => {

}

const refresh = async (req, res, next) => {

}

const grant = async (req, res, next) => {

}

module.exports = {
  register,
  verify,
  password,
  reset,
  login,
  refresh,
  grant,
}