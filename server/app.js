require('dotenv').config()
const config = require('./app-config')
const express = require('express')
const bodyParser = require('body-parser')
const app = express()

// MIDDLEWARE
const errors = require('./lib/middleware/errors')
const headers = require('./lib/middleware/headers')
const router = require('./lib/middleware/router')
const { middleware: mysql } = require('./lib/middleware/mysql')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(headers(config))
app.use(mysql(config.database.mysql))

const routes = require('./app-routes')
app.use('/', router(routes))

app.use(errors(config))

app.listen(config.port, () => {
  console.log(`Server running on port ${config.port}`)
})
